package com.fisa.omnia.mobile.firebase;

import com.fisa.omnia.mobile.CloudMessageService;
import com.fisa.util.cache.provider.HazelcastProvider;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Optional;

public class FireBaseImpl implements CloudMessageService {

    //todo:externalize to config file
    private final static String AUTH_KEY_FCM = "AAAAW1i_9Ug:APA91bFZj_mrHNJ49JpPe51u9Da99ii6gedN9yLo_gPzOi8sQ00A3bQ7MMmqS62zkxVJNIuZ_0EFYTY0D61GXc_8Kpb5nmPjTdXGAvoAZhrfmd48BR6bgT6mqPeEz7Z0wVKVwG2dr962";
    private final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
    private final static String FCM_USERS = "FCM_USERS";
    private final static HazelcastProvider cache = HazelcastProvider.getInstance();
    private HttpURLConnection conn;

    @Override
    @SuppressWarnings("unchecked")
    public void susbscribe(Long userId, String fcmToken) {
        HashMap<Long, String> fcmMap;
        if (Optional.ofNullable(cache.getCachedObject(FCM_USERS)).isPresent()) {
            fcmMap = (HashMap<Long, String>) cache.getCachedObject(FCM_USERS);
        } else {
            fcmMap = new HashMap<>();
        }
        fcmMap.put(userId, fcmToken);
        cache.putObjectInCache(FCM_USERS, fcmMap);
    }

    @Override
    public String sendMessge(Long userId, String message) throws IOException {

        this.conn = this.createFCMConnection();
        String data = this.buildPayload(userId, message);
        return this.sendRequest(data);

    }

    private String sendRequest(String data) throws IOException {
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(data);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    @SuppressWarnings("unchecked")
    private String buildPayload(Long userId, String message) {
        String deviceId = ((HashMap<Long, String>) cache.getCachedObject(FCM_USERS)).get(userId);
        if (!Optional.ofNullable(deviceId).isPresent()) {
            throw new RuntimeException("Unable to resolve FCM token");
        }
        HashMap data = new HashMap();
        data.put("to", deviceId);
        HashMap info = new HashMap();
        info.put("title", "Omnia");
        info.put("body", message);
        data.put("notification", info);

        Gson gson = new Gson();

        return gson.toJson(data);
    }

    private HttpURLConnection createFCMConnection() throws IOException {
        URL url = new URL(API_URL_FCM);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM);
        conn.setRequestProperty("Content-Type", "application/json");
        return conn;
    }
}
