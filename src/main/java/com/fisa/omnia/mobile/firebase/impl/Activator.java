package com.fisa.omnia.mobile.firebase.impl;

import com.fisa.omnia.mobile.CloudMessageService;
import com.fisa.omnia.mobile.firebase.FireBaseImpl;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {
    private ServiceRegistration serviceReference = null;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        serviceReference = bundleContext.registerService(CloudMessageService.class.getName(), new FireBaseImpl(), null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        System.out.println("Stopping Omnia Firebase bundle");
        if (serviceReference != null) {
            serviceReference.unregister();
            serviceReference = null;
        }
    }
}
