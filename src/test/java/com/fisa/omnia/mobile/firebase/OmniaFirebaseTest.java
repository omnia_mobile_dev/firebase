package com.fisa.omnia.mobile.firebase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;


public class OmniaFirebaseTest {
    private Long user = 74020L;

    @Before
    public void setUpTest(){
        FireBaseImpl notifier = new FireBaseImpl();
        String deviceId = "dzuv6ijVgek:APA91bHt5tYcEwsF-HMQC4a8h29zEMG5RD1e-OaknJNzJ7waRt03YyOfLhFy1j7T2H39hsyfMyXKHstjLSJ23ES4rO0vl8Maibus3O4zGk6g030hA-Omdxlpq-soz2MitrVSyHhVrHuk";
        notifier.susbscribe(user, deviceId);
    }

    @Test
    public void notifyTest() throws IOException {
        FireBaseImpl notifier = new FireBaseImpl();
        String result = notifier.sendMessge(user,"Manden a las casas!");
        System.out.println("Resonse: " + result);
        Assert.assertNotNull(result);
    }
}
